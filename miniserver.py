
from http.server import SimpleHTTPRequestHandler
import socketserver

class JSHandler(SimpleHTTPRequestHandler):
    def end_headers(self):
        self.send_header('Access-Control-Allow-Origin', '*')
        super().end_headers()

Handler = JSHandler

with socketserver.TCPServer(("", 8000), Handler) as httpd:
    print("Server started at http://localhost:8000")
    httpd.serve_forever()


