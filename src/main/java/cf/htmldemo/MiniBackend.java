package cf.htmldemo;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static spark.Spark.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spark.Spark;

public class MiniBackend {

    private static Logger LOGGER = LoggerFactory.getLogger(MiniBackend.class);

    public static List<String> users = new ArrayList<>();

    public static void main(String[] args) {

        final String[] allDemo = { "Running website on port: {-}\nCheck out the www pages: \n",
                "http://localhost:{-}/hello",
                "http://localhost:{-}/demo",
                "http://localhost:{-}/form",
                "http://localhost:{-}/js\n",
                "http://localhost:{-}/users",
                "http://localhost:{-}/jsclicks",
                "http://localhost:{-}/jspaint",
                "http://localhost:{-}/testcss\n",
                "http://localhost:{-}/testjq",
                "http://localhost:{-}/datejq",
                "http://localhost:{-}/bootstrap",
                "http://localhost:{-}/carousel",
                "http://localhost:{-}/accordion\n",
                "http://localhost:{-}/jsevents",
                "http://localhost:{-}/jsanimate",
                "http://localhost:{-}/jscolors"
        };

        // staticFileLocation("./");
        staticFileLocation("/public");

        port(8080);                // 4567 is the default
        init();

        final int sparPort = Spark.port();

        LOGGER.debug(  String.join( "\t", allDemo )
                .replaceAll( "\\{\\-\\}", ""+sparPort ) );

        // home page
        get("/hello", (req, res) -> {
            System.out.println(req.headers("User-Agent"));
            return "<h1>Hello World</h1>";
        });

        // display form
        get("/demo", (req, res) -> {
            String htmlFile = "target/classes/01_html/index.html";

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // provide js scripts
        get("/script.js", (req, res) -> {
            String htmlFile = "target/classes/05_js/script.js";

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // run js as backend
        get("/js", (req, res) -> {
            String htmlFile = "target/classes/05_js/index.html";

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // display form
        get("/form", (req, res) -> {
            String htmlFile = "target/classes/01_html/sparkform/index.html";

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // display CSS testing page
        get("/testcss", (req, res) -> {
            String htmlFile = "target/classes/02_css/index.html";

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // endpoint for style.css
        get("/style.css", (req, res) -> {
            String cssFile = "target/classes/02_css/style.css";
            res.type("text/css");

            LOGGER.info( cssFile );
            return Files.readString(Paths.get(cssFile));
        });

        // handle form
        post("/form-handler", (req, res) -> {
            String[] userInfo = req.body().split("&");
            String username = "";
            for (String userProp : userInfo) {
                if ((userProp.split("="))[0].equals("username")) {
                    username = userProp.split("=")[1];

                    LOGGER.info( "User name I get from form: {}", username );

                }
            }
            users.add(username);
            System.out.println(URLDecoder.decode(req.body(), StandardCharsets.UTF_8));

            return URLDecoder.decode(req.body(), StandardCharsets.UTF_8);
        });

        get("/users", (req, res) -> {
            return users;
        });

        // display JS Clicks testing page
        get("/jsclicks", (req, res) -> {
            String htmlFile = "target/classes/06_dom/index.html";

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // display JS paint testing page
        get("/jspaint", (req, res) -> {
            String htmlFile = "target/classes/07_paint/paint.html";

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // display jQuery for datepicker testing page
        get("/datejq", (req, res) -> {
            String htmlFile = "target/classes/09_jquery/jsamples_01.html";

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // display jQuery testing page
        get("/testjq", (req, res) -> {
            String htmlFile = "target/classes/09_jquery/jsamples_02.html";

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // display JS Event testing page
        get("/jsevents", (req, res) -> {
            String htmlFile = "target/classes/09_jquery/jsamples_03.html";

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // display JS Animations testing page
        get("/jsanimate", (req, res) -> {
            String htmlFile = "target/classes/09_jquery/jsamples_04.html";

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // display JS Colors testing page
        get("/jscolors", (req, res) -> {
            String htmlFile = "target/classes/09_jquery/jsamples_05.html";

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // display JS Colors testing page
        get("/bootstrap", (req, res) -> {
            String htmlFile = "target/classes/pokercc-website/index.html";

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // display JS Colors testing page
        get("/accordion", (req, res) -> {
            String htmlFile = "target/classes/03_bootstrap/bsamples_01.html";

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // display JS Colors testing page
        get("/carousel", (req, res) -> {
            String htmlFile = "target/classes/03_bootstrap/bsamples_02.html";

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // endpoint for card games site
        get("/cardgames.html", (req, res) -> {
            String htmlFile = "target/classes/pokercc-website/cardgames.html";
            res.type("text/html");

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // endpoint for card games site
        get("/dingdong.html", (req, res) -> {
            String htmlFile = "target/classes/pokercc-website/dingdong.html";
            res.type("text/html");

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // endpoint for card games site
        get("/index.html", (req, res) -> {
            String htmlFile = "target/classes/pokercc-website/index.html";
            res.type("text/html");

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // endpoint for card games site
        get("/login.html", (req, res) -> {
            String htmlFile = "target/classes/pokercc-website/login.html";
            res.type("text/html");

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });

        // endpoint for card games site
        get("/register.html", (req, res) -> {
            String htmlFile = "target/classes/pokercc-website/register.html";
            res.type("text/html");

            LOGGER.info( htmlFile );
            return Files.readString(Paths.get(htmlFile));
        });




    }
}
